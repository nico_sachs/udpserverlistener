__author__ = 'nicosachs'
import SocketServer
import threading
import Queue
import time


class MyUDPHandler(SocketServer.BaseRequestHandler):
    """
    This class works similar to the TCP handler class, except that
    self.request consists of a pair of data and client socket, and since
    there is no connection the client address must be given explicitly
    when sending data back via sendto().
    """

    def handle(self):
        data = self.request[0].strip()
        socket = self.request[1]
        #print "{} wrote:".format(self.client_address[0])
        #print data
        q.put(data)
        socket.sendto("OK", self.client_address)


class MyListenerThread(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)

    def run(self):
        HOST, PORT = "localhost", 2000
        server = SocketServer.UDPServer((HOST, PORT), MyUDPHandler)
        server.serve_forever()

q = Queue.Queue()

thread = MyListenerThread()
thread.start()

while 1:
    while not q.empty():
        print q.get()
    time.sleep(1)


